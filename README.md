# Enhance Your Kubernetes CI/CD Pipelines with GitLab & Open Source

This project contains sample code related to my talk at GitLab Commit San Francisco.

This repo contains a GitLab Serverless sample.

Test with:
```
curl \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"GitLab":"FaaS"}' \
  http://<function-url>?[1-1000]
```

```
wrk \
  -c 100 \
  -t 50 \
  -d 60s \
  http://<function-url>
```
